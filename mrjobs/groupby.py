# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 21:32:13 2015
Read purchases.txt & find price groupby store
@author: chirgal
"""
from mrjob.job import MRJob



class groupby(MRJob):
    def mapper(self, _, line):
        yield line.split("\t")[2],float(line.split("\t")[4])
   

    def reducer(self,key,values):
        yield key,sum(values)        

if __name__=='__main__':
    groupby.run()