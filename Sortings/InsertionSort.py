#Insertion sort
#Run online @http://ideone.com/yisTK4
import unittest
def InsertionSort(myList):
  for index in xrange(1,len(myList)):
                currentValue=myList[index]
                pos=index
		
                while (pos>0) and (myList[pos-1]>currentValue):
                        myList[pos]=myList[pos-1]
                        pos=pos-1
			
                        myList[pos]=currentValue
		
  return myList

class uTest(unittest.TestCase):
        def test1(self):
                my_list = [54,26]
                self.assertEqual(InsertionSort(my_list),[26,54])
	
        def test2(self):
                my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
                self.assertEqual(InsertionSort(my_list),[17, 20, 26, 31, 44, 54, 55, 77, 93])	
		
	     
if __name__=="__main__":
    unittest.main()
			
	
