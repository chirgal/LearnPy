# -*- coding: utf-8 -*-
"""
Created on Tue May 17 22:16:33 2016

"""
import json
import os


def get_decoded(input_data):
    decoder = json.JSONDecoder()
    obj, end = decoder.raw_decode(input_data)
    return obj

def flattenDict(d,result=None,index=None,Key=None,keySep="/"):
    newkeySep=keySep
    if result is None:
        result = {}
    if isinstance(d, (list, tuple)):
        for indexB, element in enumerate(d):
            if Key is not None:
                newkey = Key
            flattenDict(element,result,index=indexB,Key=newkey,keySep=newkeySep)            
    elif isinstance(d, dict):        
        for key in d:
            value = d[key]         
            if Key is not None and index is not None:
                newkey = keySep.join([Key,str(index),(str(key).replace(" ", ""))])
                #newkey = ".".join([Key,(str(key).replace(" ", "") + str(index))])
            elif Key is not None:
                newkey = keySep.join([Key,(str(key).replace(" ", ""))])
            else:
                newkey= str(key).replace(" ", "")
            flattenDict(value,result,index=None,Key=newkey,keySep=newkeySep)        
    else:
        result[Key]=d        
    return result
    
fp=open('/home/chirgal/Documents/TestData/filename.json','r')
data=fp.read().strip(os.linesep)

fdata=get_decoded(data)

result= flattenDict(fdata,keySep=".")


for key in sorted(result):
    print('%s=%s' %(key,result[key]))
