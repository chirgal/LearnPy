# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 18:53:15 2016

@author: chirgal
"""

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String,Text,DateTime,Integer

engine = create_engine('mysql+pymysql://root@localhost/locanto?charset=utf8')

Base = declarative_base()

class Post(Base):
       __tablename__ = 'posts'
       
       id =   Column(String(20), primary_key=True)
       epoc = Column(String(20), primary_key=True)
       posted_on=Column(DateTime)
       header = Column(String(150))
       descp = Column(String(300))
       contnt= Column(Text)
       user=Column(String(150))
       phone= Column(String(60))
       url = Column(String(150))
       cat = Column(String(100))
       
       def __init__(self,id,epoc,posted_on,header,descp,contnt,user,phone,url,cat):
           self.id=id
           self.epoc=epoc
           self.posted_on=posted_on
           self.header=header
           self.descp=descp
           self.contnt=contnt
           self.user=user
           self.phone=phone
           self.url=url
           self.cat=cat
class Buser(Base):
      __tablename__='blocked_user'
      
      id =   Column(Integer, primary_key=True)
      user=Column(String(150))
           
Base.metadata.create_all(engine)       