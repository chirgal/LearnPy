#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 17:33:55 2016

@author: chirgal
"""
import threading
from threading import Thread
from bs4 import BeautifulSoup
from datetime import datetime
import urllib2
from urlparse import urlparse
import os
import re
import os.path
from sqlalchemy.orm import sessionmaker
from model import *

threadLock = threading.Lock()
skipped_ad_count=0
new_ad_count=0

#main_url='http://bhubaneswar.locanto.in/Personals/P/'
regex=re.compile('\d{10,12}')
#Session = sessionmaker(bind=engine)
#s=Session()

def fetchUrl(url):
    global skipped_ad_count
    global new_ad_count
    global threadLock
    Session = sessionmaker(bind=engine)
    s=Session()
    
    res=urllib2.urlopen(url)
    data=res.read()
    res.close()
    
    soup=BeautifulSoup(data,"lxml")
    #soup=BeautifulSoup(open('/home/chirgal/data.html','rb'),"lxml")
    entries=soup.find("div",class_="entries")
    rows=entries.find_all("div",class_="resultRow")
    for row in rows:
        try:
            post_url=row.a['href']
            parsed_url=urlparse(post_url)
            post_id=os.path.split(parsed_url.path)[0].strip("/ID_")
            
            post_header=row.find("span",class_="textHeader").text.strip().strip('\t').strip(os.linesep).replace("\n"," ")
            #sys.stdout.write("\rSaving Ad: %s" % post_header)
            post_desc=row.find("span",class_="textDesc").text.strip().strip('\t').strip(os.linesep).replace("\n"," ")
            post_phone_list=regex.findall(post_desc)
            if post_phone_list is None:
                post_phone=''
            else:
                post_phone=','.join(post_phone_list)
                #print("Got Number: %s"%post_phone)
            post_cat=row.find("div",class_="resultCat").text.strip(os.linesep).replace("\n"," ")
            post_unix=row['data-unix']
            posted_on=datetime.fromtimestamp(int(post_unix))
            
            #threadLock.acquire()
            if s.query(Post).filter(Post.id ==post_id).first() is not None:
                skipped_ad_count+=1
                #threadLock.release()
                #print("Skipping Ad: %s  [%s]" % (post_header,post_id))
                
                continue
                
            else:
                
                
                
                #threadLock.release()
                res2=urllib2.urlopen(post_url)
                data2=res2.read()
                res2.close()
                soup2=BeautifulSoup(data2,"lxml")
                
                
                try:
                   post_user=soup2.find('a',class_="vap_sidebox_username" ).next_element.strip()
                   
                   #post_user=os.path.split(urlparse(post_user).path)[1].strip()
                except:
                   post_user=soup2.find('div',class_="vap_sidebox_username" ).next_element.strip()
                else:   
                   blocked_user=s.query(Buser).filter(Buser.user==post_user.upper()).first()
                   if blocked_user is not None:
                       skipped_ad_count+=1
                       print("Skipping Ad: %s  [%s] [%s]" % (post_header,post_id,post_user))
                       continue
        except AttributeError:
            continue
        else:
            
            
            post=Post(post_id,post_unix,posted_on,post_header,post_desc,"",post_user,post_phone,post_url,post_cat)
            s.merge(post)
            s.commit()
            new_ad_count+=1
            print("Saved Ad: %s  [%s] [%s]" %(post_header,post_id,post_user))
        
def getUrl(url):
    paging_urls=[]
    res=urllib2.urlopen(url)
    data=res.read()
    res.close()
    
    soup=BeautifulSoup(data,"lxml")
    #soup=BeautifulSoup(open('/home/chirgal/data.html','rb'),"lxml")
    paging=soup.find("div",class_="paging")
    url_tags=paging.find_all('a')
    
    for url_tag in url_tags:
        paging_urls.append(url_tag['href'])
    return paging_urls    

def processUrl(main_url):                           
            processed_urls=[]
            urls=getUrl(main_url)
            urls.insert(0,main_url)
            #print (urls)
            for url in urls:
                if url not in processed_urls:
#                    t=Thread(target=fetchUrl,args=(url,))
#                    t.start()
#                    t.join()
                    fetchUrl(url)
                    processed_urls.append(url)
if __name__=="__main__":
    
    
    main_urls=['http://bhubaneswar.locanto.in/Women-Seeking-Men/202/',
                                'http://bhubaneswar.locanto.in/Women-Looking-for-Men/20702/',
                                'http://bhubaneswar.locanto.in/Escorts/20905/',]
     
#'http://bhubaneswar.locanto.in/Women-Seeking-Men/202/',
                    
        #print(processed_urls)
    for main_url in main_urls:
                    #processUrl(main_url)
                    t=Thread(target=processUrl,args=(main_url,))
                    t.start()
                    t.join()
                       
    print("Skipped Ad count:%i"%skipped_ad_count) 
    print("New Ad count:%i"%new_ad_count)        

            
        
    
    






