# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 11:49:01 2016

@author: Chirgal.Hansdah
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tabledef import *
 
engine = create_engine('sqlite:///tutorial.db', echo=True)
 
# create a Session
Session = sessionmaker(bind=engine)
session = Session()
 
user = User("admin@gmail.com","password")
session.add(user)
 
user = User("python@gmail.com","python")
session.add(user)
 
user = User("jumpiness@gmail.com","python")
session.add(user)
 
# commit the record the database
session.commit()
