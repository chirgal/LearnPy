# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 11:14:56 2016

@author: Chirgal.Hansdah
"""

class Celsius:
    def __init__(self, temperature = 0):
        self.__temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self.__temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self.__temperature = value
        
