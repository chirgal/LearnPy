# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:13:11 2016

@author: Chirgal.Hansdah
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 11:36:18 2016

@author: Chirgal.Hansdah
"""


from sqlalchemy import create_engine
from sqlalchemy import Column, String,Boolean,Integer,Sequence,DECIMAL,Date
from sqlalchemy.ext.declarative import declarative_base
 
engine = create_engine('sqlite:///expenstica_test.db', echo=True)
Base = declarative_base()
 
########################################################################
class User(Base):
    """"""
    __tablename__ = "users_test"
    
    

    id=Column(Integer, primary_key=True)    
    firstname=Column(String(25))
    lastname=Column(String(25))
    email = Column(String(25), unique=True,index=True)
    password = Column(String(100))
    authenticated = Column(Boolean, default=False)
 
    def __init__(self ,firstname,lastname,email ,password):
        
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password
        
 
    def is_authenticated(self):
        return True
 
    def is_active(self):
        return True
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return unicode(self.id)
 
    def __repr__(self):
        return '<User %r>' % (self.username)

# create tables
Base.metadata.create_all(engine)