# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 11:36:18 2016

@author: Chirgal.Hansdah
"""


from sqlalchemy import create_engine,ForeignKey
from sqlalchemy import Column, String,Boolean,Integer,Sequence,DECIMAL,Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship,backref
 
##engine = create_engine('sqlite:///expenstica.db', echo=True)
engine = create_engine('mysql+pymysql://root@localhost/accelerate')
Base = declarative_base()
 
########################################################################
class User(Base):
    """"""
    __tablename__ = "users"
    
    

    id=Column(Integer, primary_key=True)    
    firstname=Column(String(25),nullable=False)
    lastname=Column(String(25),nullable=False)
    email = Column(String(35), index=True,unique=True)
    password = Column(String(100),nullable=False)
    authenticated = Column(Boolean, default=False)
 
    def __init__(self ,firstname,lastname,email ,password):
        
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password
        
 
    def is_authenticated(self):
        return True
 
    def is_active(self):
        return True
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return unicode(self.id)
 
    def __repr__(self):
        return '<User %r>' % (self.username)


class Expense(Base):
    __tablename__='expenses'
    
    id=Column(Integer,primary_key=True)
    catagory=Column(String(35))
    details=Column(String(35))
    amount=Column(DECIMAL(8,2))
    date=Column(Date)
    user_id = Column(Integer, ForeignKey('users.id'))
    users = relationship("User", backref=backref("users", order_by=id))
    
    def __init__(self,catagory,details,amount,date,user_id):
        self.catagory=catagory
        self.details=details
        self.amount=amount
        self.date=date
        self.user_id=user_id



# create tables
Base.metadata.create_all(engine)